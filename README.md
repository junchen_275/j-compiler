# J-- Compiler

Implemented extensions of j-- compiler (for example, interfaces, additional control statements, Exception handling, doubles, floats and longs); understood how compilers work, how to write compilers, and how the Java language behaves.