package pass;

import java.lang.Integer;
import java.lang.System;

public class Sum {
    private static String MGS = "SUM = ";
    private int n;

    public Sum(int n) {
        this.n = n;
    }

    public int compute() {
        int sum = 0, i = n;
        while (i > 0) {
            sum += i--;
        }
        return sum;
    }
    public static void main(String[] args) {
        int n = Integer.arseInt(args[0]);
        Sum sum = new Sum(n);
        System.out.println(MSG + sum.compute());
    }
}