{
    "JCompilationUnit:1":
    {
        "source": "exam/exam1.java",
        "package": "pass",
        "imports": ["java.lang.Integer", "java.lang.System"],
        "CompilationUnitContext":
        {
            "entries": ["Integer", "java.lang.System", "pass.Sum", "Object", "java.lang.Object", "Sum", "String", "java.lang.String", "java.lang.Integer", "System"]
        },
        "JClassDeclaration:6":
        {
            "modifiers": ["public"],
            "name": "Sum",
            "super": "java.lang.Object",
            "ClassContext":
            {
            },
            "JFieldDeclaration":
            {
                "line": "7",
                "modifiers": ["private", "static"],
                "VariableDeclarators":
                {
                    "JVariableDeclarator:7":
                    {
                        "name": "MGS",
                        "type": "java.lang.String",
                        "Initializer":
                        {
                            "JLiteralString:7":
                            {
                                "type": "",
                                "value": "SUM = "
                            }
                        }
                    }
                }
            },
            "JFieldDeclaration":
            {
                "line": "8",
                "modifiers": ["private"],
                "VariableDeclarators":
                {
                    "JVariableDeclarator:8":
                    {
                        "name": "n",
                        "type": "int"
                    }
                }
            },
            "JConstructorDeclaration:10":
            {
                "name": "Sum",
                "modifiers": ["public"],
                "parameters": [["n", "int"]],
                "JBlock:10":
                {
                    "JStatementExpression:11":
                    {
                        "JBinaryExpression:11":
                        {
                            "operator": "=",
                            "type": "",
                            "Operand1":
                            {
                                "JFieldSelection:11":
                                {
                                    "ambiguousPart": "null",
                                    "name": "n",
                                    "Target":
                                    {
                                        "JThis:11":
                                        {
                                        }
                                    }
                                }
                            },
                            "Operand2":
                            {
                                "JVariable:11":
                                {
                                    "name": "n"
                                }
                            }
                        }
                    }
                }
            },
            "JMethodDeclaration:14":
            {
                "name": "compute",
                "returnType": "int",
                "modifiers": ["public"],
                "parameters": [],
                "JBlock:14":
                {
                    "JVariableDeclaration:15":
                    {
                        "JVariableDeclarator:15":
                        {
                            "name": "sum",
                            "type": "int",
                            "Initializer":
                            {
                                "JLiteralInt:15":
                                {
                                    "type": "",
                                    "value": "0"
                                }
                            }
                        },
                        "JVariableDeclarator:15":
                        {
                            "name": "i",
                            "type": "int",
                            "Initializer":
                            {
                                "JVariable:15":
                                {
                                    "name": "n"
                                }
                            }
                        }
                    },
                    "JWhileStatement:16":
                    {
                        "Condition":
                        {
                            "JBinaryExpression:16":
                            {
                                "operator": ">",
                                "type": "",
                                "Operand1":
                                {
                                    "JVariable:16":
                                    {
                                        "name": "i"
                                    }
                                },
                                "Operand2":
                                {
                                    "JLiteralInt:16":
                                    {
                                        "type": "",
                                        "value": "0"
                                    }
                                }
                            }
                        },
                        "Body":
                        {
                            "JBlock:16":
                            {
                                "JStatementExpression:17":
                                {
                                    "JBinaryExpression:17":
                                    {
                                        "operator": "+=",
                                        "type": "",
                                        "Operand1":
                                        {
                                            "JVariable:17":
                                            {
                                                "name": "sum"
                                            }
                                        },
                                        "Operand2":
                                        {
                                            "JUnaryExpression:17":
                                            {
                                                "operator": "-- (post)",
                                                "type": "",
                                                "Operand":
                                                {
                                                    "JVariable:17":
                                                    {
                                                        "name": "i"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "JReturnStatement:19":
                    {
                        "Expression":
                        {
                            "JVariable:19":
                            {
                                "name": "sum"
                            }
                        }
                    }
                }
            },
            "JMethodDeclaration:21":
            {
                "name": "main",
                "returnType": "void",
                "modifiers": ["public", "static"],
                "parameters": [["args", "java.lang.String[]"]],
                "JBlock:21":
                {
                    "JVariableDeclaration:22":
                    {
                        "JVariableDeclarator:22":
                        {
                            "name": "n",
                            "type": "int",
                            "Initializer":
                            {
                                "JMessageExpression:22":
                                {
                                    "ambiguousPart": "Integer",
                                    "name": "arseInt",
                                    "Argument":
                                    {
                                        "JArrayExpression:22":
                                        {
                                            "TheArray":
                                            {
                                                "JVariable:22":
                                                {
                                                    "name": "args"
                                                }
                                            },
                                            "TheIndex":
                                            {
                                                "JLiteralInt:22":
                                                {
                                                    "type": "",
                                                    "value": "0"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "JVariableDeclaration:23":
                    {
                        "JVariableDeclarator:23":
                        {
                            "name": "sum",
                            "type": "Sum",
                            "Initializer":
                            {
                                "JNewOp:23":
                                {
                                    "type": "Sum",
                                    "Argument":
                                    {
                                        "JVariable:23":
                                        {
                                            "name": "n"
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "JStatementExpression:24":
                    {
                        "JMessageExpression:24":
                        {
                            "ambiguousPart": "System.out",
                            "name": "println",
                            "Argument":
                            {
                                "JBinaryExpression:24":
                                {
                                    "operator": "+",
                                    "type": "",
                                    "Operand1":
                                    {
                                        "JVariable:24":
                                        {
                                            "name": "MSG"
                                        }
                                    },
                                    "Operand2":
                                    {
                                        "JMessageExpression:24":
                                        {
                                            "ambiguousPart": "sum",
                                            "name": "compute"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
