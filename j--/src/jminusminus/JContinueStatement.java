package jminusminus;

import static jminusminus.CLConstants.*;

/**
 * The AST node for Continue.
 */
class JContinueStatement extends JStatement {

    /**
     * Constructs an AST node for Continue.
     *
     * @param line line in which the literal occurs in the source file.
     */
    public JContinueStatement(int line) {
        super(line);
    }

    /**
     * {@inheritDoc}
     */
    public JStatement analyze(Context context) {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JContinueStatement:" + line, e);
    }
}
