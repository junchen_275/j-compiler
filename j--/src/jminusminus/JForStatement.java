package jminusminus;

import java.util.ArrayList;
import static jminusminus.CLConstants.*;

class JForStatement extends JStatement {
    // the list of initializations
    private ArrayList<JStatement> initializations;

    // Test expression.
    private JExpression condition;

    // the list of updates
    private ArrayList<JStatement> updates;

    // The body.
    private JStatement body;

    //LocalContext for analyze
    private LocalContext localContext;

    /**
     * Constructs an AST node for a for-statement.
     *
     * @param line      line in which the for-statement occurs in the source file.
     * @param initialization list of JVariableDeclaration.
     * @param condition expression.
     * @param updates   ArrayList<JStatement> updates
     * @param body      the body.
     */
    public JForStatement(int line, ArrayList<JStatement> initializations, JExpression condition,
                           ArrayList<JStatement> updates, JStatement body) {
        super(line);
        this.initializations = initializations;
        this.condition = condition;
        this.updates = updates;
        this.body = body;
    }

    /**
     * {@inheritDoc}
     */
    public JForStatement analyze(Context context) {
        context.addEntry(decl.line(), decl.name(), defn);
        if (initializations != null) {
            for (JStatement init : initializations) {
                init = (JStatement) init.analyze(localContext);
            }
        }
        condition = condition.analyze(localContext);
        condition.type().mustMatchExpected(line(), Type.BOOLEAN);
        if (updates != null) {
            for (JStatement update : updates) {
                update = (JStatement) update.analyze(localContext);
            }
        }
        body = (JStatement) body.analyze(localContext);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        String test = output.createLabel();
        String out = output.createLabel();

        if (initializations != null) {
            for (JStatement init : initializations) {
                init.codegen(output);
            }
        }
        output.addLabel(test);

        condition.codegen(output, out, false);

        body.codegen(output);

        if (updates != null) {
            for (JStatement update : updates) {
                update.codegen(output);
            }
        }
        output.addBranchInstruction(GOTO, test);
        output.addLabel(out);
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JForStatement:" + line, e);
        JSONElement e1 = new JSONElement();
        e.addChild("Init", e1);
        if (initializations != null) {
            for (JStatement initialization : initializations) {
                initialization.toJSON(e1);
            }
        }
        JSONElement e2 = new JSONElement();
        e.addChild("Condition", e2);
        condition.toJSON(e2);
        JSONElement e3 = new JSONElement();
        e.addChild("Update", e3);
        if (updates != null) {
            for (JStatement update : updates) {
                update.toJSON(e3);
            }
        }
        JSONElement e4 = new JSONElement();
        e.addChild("Body", e4);
        body.toJSON(e4);
    }
}

/**
    implement the forInit to be able to work with statementExpressions and variablesDeclarators.
 */
class JForInitDeclaration extends JStatement {

    private ArrayList<JVariableDeclarator> declarators;

    public JForInitDeclaration(int line, ArrayList<JVariableDeclarator> declarators) {
        super(line);
        this.declarators = declarators;
    }

    public JForInitDeclaration analyze(Context context) {

        return this;
    }

    public void codegen (CLEmitter output) {

    }

    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JVariableDeclaration:" + line, e);
        for (JVariableDeclarator decl : declarators) {
            decl.toJSON(e);
        }
    }

}