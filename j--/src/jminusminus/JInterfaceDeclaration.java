// Copyright 2012- Bill Campbell, Swami Iyer and Bahar Akbal-Delibas
/*
package jminusminus;

import java.util.ArrayList;

import static jminusminus.CLConstants.*;

/**
 * A representation of a interface declaration.

class JClassDeclaration extends JAST implements JTypeDecl {
    // interface modifiers.
    private ArrayList<String> mods;

    // interface name.
    private String name;

    // interface block.
    private ArrayList<JMember> classBlock;

    // This interface type.
    private Type thisType;

    // Super class type.
    private Type superType;

    // Context for this class.
    private ClassContext context;

    // Whether this class has an explicit constructor.
    private boolean hasExplicitConstructor;

    // Instance fields of this class.
    private ArrayList<JFieldDeclaration> instanceFieldInitializations;

    // Static (class) fields of this class.
    private ArrayList<JFieldDeclaration> staticFieldInitializations;

    /**
     * Constructs an AST node for a class declaration.
     *
     * @param line       line in which the class declaration occurs in the source file.
     * @param mods       class modifiers.
     * @param name       class name.
     * @param superType  super class type.
     * @param classBlock class block.

    public JInterfaceDeclaration(int line, ArrayList<String> mods, String name, Type superType,
                             ArrayList<JMember> classBlock) {
        super(line);
        this.mods = mods;
        this.name = name;
        this.superType = superType;
        this.classBlock = classBlock;
        hasExplicitConstructor = false;
        instanceFieldInitializations = new ArrayList<JFieldDeclaration>();
        staticFieldInitializations = new ArrayList<JFieldDeclaration>();
    }

    /**
     * Returns the initializations for instance fields (expressed as assignment statements).
     *
     * @return the initializations for instance fields (expressed as assignment statements).

    public ArrayList<JFieldDeclaration> instanceFieldInitializations() {
        return instanceFieldInitializations;
    }

    /**
     * {@inheritDoc}

    public void declareThisType(Context context) {
        String qualifiedName = JAST.compilationUnit.packageName() == "" ?
                name : JAST.compilationUnit.packageName() + "/" + name;
        CLEmitter partial = new CLEmitter(false);
        partial.addClass(mods, qualifiedName, Type.OBJECT.jvmName(), null, false);
        thisType = Type.typeFor(partial.toClass());
        context.addType(line, thisType);
    }

    /**
     * {@inheritDoc}

    public void preAnalyze(Context context) {

    }

    /**
     * {@inheritDoc}

    public String name() {
        return name;
    }

    /**
     * {@inheritDoc}

    public Type thisType() {
        return thisType;
    }

    /**
     * {@inheritDoc}

    public Type superType() {
        return superType;
    }

    /**
     * {@inheritDoc}

    public JAST analyze(Context context) {

        return this;
    }

    /**
     * {@inheritDoc}

    public void codegen(CLEmitter output) {

    }

    /**
     * {@inheritDoc}

    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JClassDeclaration:" + line, e);
        if (mods != null) {
            ArrayList<String> value = new ArrayList<String>();
            for (String mod : mods) {
                value.add(String.format("\"%s\"", mod));
            }
            e.addAttribute("modifiers", value);
        }
        e.addAttribute("name", name);
        e.addAttribute("super", superType == null ? "" : superType.toString());
        if (context != null) {
            context.toJSON(e);
        }
        if (classBlock != null) {
            for (JMember member : classBlock) {
                ((JAST) member).toJSON(e);
            }
        }
    }

    // Generates code for an implicit empty constructor (necessary only if there is not already
    // an explicit one).
    private void codegenPartialImplicitConstructor(CLEmitter partial) {

    }

    // Generates code for an implicit empty constructor (necessary only if there is not already
    // an explicit one).
    private void codegenImplicitConstructor(CLEmitter output) {
    }

    // Generates code for class initialization (in j-- this means static field initializations.
    private void codegenClassInit(CLEmitter output) {
}
*/