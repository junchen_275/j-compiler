
package jminusminus;

import static jminusminus.CLConstants.*;
import java.util.ArrayList;

/**
 * The AST node for an switch-statement.
 */

class JSwitchStatement extends JStatement {

    // condition.
    private JExpression parExpression;

    // list of SwitchStatementGroup objects.
    private ArrayList<JSwitchBlockStatementGroup> switchBlockStatement;

/**
     * Constructs an AST node for an switch-statement.
     *
     * @param line      line in which the switch-statement occurs in the source file.
     * @param parExpression condition.
     * @param switchBlockStatementGroup  the list of SwitchStatementGroup objects.
     */
    public JSwitchStatement(int line, JExpression parExpression,
                            ArrayList<JSwitchBlockStatementGroup> switchBlockStatement) {
        super(line);
        this.parExpression = parExpression;
        this.switchBlockStatement = switchBlockStatement;
    }

/**
     * {@inheritDoc}
     */

    public JSwitchStatement analyze(Context context) {
        return this;
    }

/**
     * {@inheritDoc}
     */

    public void codegen(CLEmitter output) {
    }

/**
     * {@inheritDoc}
     */

    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JSwitchStatement:" + line, e);
        JSONElement e1 = new JSONElement();
        e.addChild("Condition", e1);
        parExpression.toJSON(e1);
        for (JSwitchBlockStatementGroup block: switchBlockStatement) {
            JSONElement e2 = new JSONElement();
            e.addChild("SwitchStatementGroup", e2);
            block.toJSON(e2);
        }
    }
}

/**
 * it returns an object of SwitchStatementGroup that encapsulates two arrays.
 */

class JSwitchBlockStatementGroup extends JStatement {

    //an array of labels
    private ArrayList<JExpression> labels;

    //an array of block statements
    private ArrayList<JStatement> blockStatements;

    public JSwitchBlockStatementGroup(int line, ArrayList<JExpression> labels, ArrayList<JStatement> blockStatements) {
        super(line);
        this.labels = labels;
        this.blockStatements = blockStatements;
    }

    public JSwitchBlockStatementGroup analyze(Context context) {
        return this;
    }

    public void codegen(CLEmitter output) {
    }

    public void toJSON(JSONElement json) {
        for (JExpression expression : labels) {
            if (expression instanceof JDefaultStatement) {
                JSONElement e = new JSONElement();
                json.addChild("Default", e);
                expression.toJSON(e);
            } else {
                JSONElement e = new JSONElement();
                json.addChild("Case", e);
                expression.toJSON(e);
            }
        }
        for (JStatement statement: blockStatements) {
            statement.toJSON(json);
        }
    }
}

class JDefaultStatement extends JExpression {
    public JDefaultStatement(int line) {
        super(line);
    }

    /**
     * {@inheritDoc}
     */
    public JExpression analyze(Context context) {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
    }

    /**
     * {@inheritDoc}
     */
    /*
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("Default", e);
    }
     */
}